data.headers = [
    {
        style: {width: String(960 * .05) + 'px', extra: { }},
        title: 'more',
        displayTitle: '',
        display: false,
        render: function() {
            return this.displayTitle;
        }
    },
    {
        style: {width: String(960 * .2) + 'px', border: '1px solid black', 'text-decoration': 'underline'},
        title: 'model',
        displayTitle: 'model',
        display: true,
        render: function() {
            return this.displayTitle;
        }
    },
    {
        style: {width: String(960 * .3) + 'px', border: '1px solid black', 'text-decoration': 'underline'},
        title: 'manufacturer',
        displayTitle: 'manufacturer',
        display: true,
        render: function() {
            return this.displayTitle;
        }
    },
    {
        style: {width: String(960 * .3) + 'px', border: '1px solid black', 'text-decoration': 'underline'},
        title: 'serial',
        displayTitle: 'serial number',
        display: true,
        render: function() {
            return this.displayTitle;
        }
    },
    {
        style: {width: String(960 * .15) + 'px', extra: {'transform':'translate3d(0px,-15px,0) rotate3d(0,0,1,-60deg)','line-height': '3rem', 'font-size':'1.2rem'}},
        title: 'statusBubbles',
        displayTitle: 'No Issues<br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Service Required<br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Out of Order',
        display: true,
        render: function(el) {
            $(el)[0].innerHTML = this.displayTitle;
        }
    }
];

// our sample data to program this application

data.elements = [
    {
        id: '1',
        loaded: false,
        status: 'in',
        model: 'CRIO-9273',
        manufacturer: 'National Instruments',
        serial: '23519B23f21a',
        view:  'item.details.html',
        description: 'Fermentum dolor ac pulvinar. Duis vehicula posuere eros, ac porttitor justo luctus non. In hac habitasse platea dictumst. Etiam vel risus lorem. Nunc in tortor quis orci efficitur venenatis. Nulla id rhoncus dolor. '
    },
    {
        id: '2',
        loaded: false,
        status: 'out',
        model: 'Tachiometer 9000',
        manufacturer: 'Hudson Products, Inc.',
        serial: 'B23f8Ab',
        view:  'item.details.html',
        description: 'Fermentum dolor ac pulvinar. Duis vehicula posuere eros, ac porttitor justo luctus non. In hac habitasse platea dictumst. Etiam vel risus lorem. Nunc in tortor quis orci efficitur venenatis. Nulla id rhoncus dolor. '
    },
    {
        id: '3',
        loaded: false,
        status: 'error',
        model: 'FlowMaster 21-B',
        manufacturer: 'Mayfield Industries',
        serial: '339-234',
        view:  'item.details.html',
        description: 'Fermentum dolor ac pulvinar. Duis vehicula posuere eros, ac porttitor justo luctus non. In hac habitasse platea dictumst. Etiam vel risus lorem. Nunc in tortor quis orci efficitur venenatis. Nulla id rhoncus dolor. '
    },
    {
        id: '4',
        loaded: false,
        status: 'in',
        model: 'CRIO-9273',
        manufacturer: 'National Instruments',
        serial: '23519B23f21a',
        view:  'item.details.html',
        description: 'Fermentum dolor ac pulvinar. Duis vehicula posuere eros, ac porttitor justo luctus non. In hac habitasse platea dictumst. Etiam vel risus lorem. Nunc in tortor quis orci efficitur venenatis. Nulla id rhoncus dolor. '
    },
    {
        id: '5',
        loaded: false,
        status: 'in',
        model: 'Tachiometer 9000',
        manufacturer: 'Hudson Products, Inc.',
        serial: 'B23f8Ab',
        view:  'item.details.html',
        description: 'Fermentum dolor ac pulvinar. Duis vehicula posuere eros, ac porttitor justo luctus non. In hac habitasse platea dictumst. Etiam vel risus lorem. Nunc in tortor quis orci efficitur venenatis. Nulla id rhoncus dolor. '
    },
    {
        id: '6',
        loaded: false,
        status: 'in',
        model: 'FlowMaster 21-B',
        manufacturer: 'Mayfield Industries',
        serial: '339-234',
        view:  'item.details.html',
        description: 'Fermentum dolor ac pulvinar. Duis vehicula posuere eros, ac porttitor justo luctus non. In hac habitasse platea dictumst. Etiam vel risus lorem. Nunc in tortor quis orci efficitur venenatis. Nulla id rhoncus dolor. '
    },
    {
        id: '7',
        loaded: false,
        status: 'in',
        model: 'CRIO-9273',
        manufacturer: 'National Instruments',
        serial: '23519B23f21a',
        view:  'item.details.html',
        description: 'Fermentum dolor ac pulvinar. Duis vehicula posuere eros, ac porttitor justo luctus non. In hac habitasse platea dictumst. Etiam vel risus lorem. Nunc in tortor quis orci efficitur venenatis. Nulla id rhoncus dolor. '
    },
    {
        id: '8',
        loaded: false,
        status: 'out',
        model: 'Tachiometer 9000',
        manufacturer: 'Hudson Products, Inc.',
        serial: 'B23f8Ab',
        view:  'item.details.html',
        description: 'Fermentum dolor ac pulvinar. Duis vehicula posuere eros, ac porttitor justo luctus non. In hac habitasse platea dictumst. Etiam vel risus lorem. Nunc in tortor quis orci efficitur venenatis. Nulla id rhoncus dolor. '
    },
    {
        id: '9',
        loaded: false,
        status: 'warning',
        model: 'FlowMaster 21-B',
        manufacturer: 'Mayfield Industries',
        serial: '339-234',
        view:  'item.details.html',
        description: 'Fermentum dolor ac pulvinar. Duis vehicula posuere eros, ac porttitor justo luctus non. In hac habitasse platea dictumst. Etiam vel risus lorem. Nunc in tortor quis orci efficitur venenatis. Nulla id rhoncus dolor. '
    },
    {
        id: '10',
        loaded: false,
        status: 'out',
        model: 'CRIO-9273',
        manufacturer: 'National Instruments',
        serial: '23519B23f21a',
        view:  'item.details.html',
        description: 'Fermentum dolor ac pulvinar. Duis vehicula posuere eros, ac porttitor justo luctus non. In hac habitasse platea dictumst. Etiam vel risus lorem. Nunc in tortor quis orci efficitur venenatis. Nulla id rhoncus dolor. '
    },
    {
        id: '11',
        loaded: false,
        status: 'in',
        model: 'Tachiometer 9000',
        manufacturer: 'Hudson Products, Inc.',
        serial: 'B23f8Ab',
        view:  'item.details.html',
        description: 'Fermentum dolor ac pulvinar. Duis vehicula posuere eros, ac porttitor justo luctus non. In hac habitasse platea dictumst. Etiam vel risus lorem. Nunc in tortor quis orci efficitur venenatis. Nulla id rhoncus dolor. '
    },
    {
        id: '12',
        loaded: false,
        status: '',
        model: 'FlowMaster 21-B',
        manufacturer: 'Mayfield Industries',
        serial: '339-234',
        view:  'item.details.html',
        description: 'Fermentum dolor ac pulvinar. Duis vehicula posuere eros, ac porttitor justo luctus non. In hac habitasse platea dictumst. Etiam vel risus lorem. Nunc in tortor quis orci efficitur venenatis. Nulla id rhoncus dolor. '
    }
];/**
 * Created by chris on 2/6/15.
 */
