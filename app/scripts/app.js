/* global angular */

var data = {};

// a description of the data's fields including display information (width, decoration, etc.)

function lazyLoad(url,callback) {
    if (/^.*js$/.exec(url)) {
        var script = document.createElement('script');
        script.src = url;
        script.onload = callback;
        document.head.appendChild(script);
    } else if (/^.*css$/.exec(url)) {
        var style = document.createElement('link');
        style.href = url;
        style.rel = 'stylesheet';
        style.onload = callback;
        document.head.appendChild(style);
    }
}

// a simple wrapper around jQuery's 'reduce' function
// to pull desired elements out of our JSON data object
// @match is a  json object of attributes to match, or a single string for an 'id' to match
// @from is the array of objects to search, or 'null' to search the global 'data' object
// @retval is an array of objects that match the filter
function findElements(match, from) {
    match = match || {};
    from = from || data;

    if (typeof match === 'string') {
        match = {id: match};
    }

    return $(from).filter(function (index, item) {
        for (var field in match) {
            if (match.hasOwnProperty(field) && match[field] !== item[field]) {
                return false;
            }
        }
        return true;
    });
}

// main angular entry point
(function (app) {

    // our controller for the data table
    function TableCtrl($scope) {

        //var active, one;

        /*
         * this function expands out a single row to display detailed information.
         */
        this.toggle = function (el) {

            if (typeof el.open === 'undefined')
                el.open = true;
            else
                el.open = !el.open;

            var next = $("#" + el.id).toggleClass('menu-open');

            //if( next.hasClass('menu-open') )
            //    $(active).removeClass('menu-open');
            //else
            //    active = null;

            el.template = 'app/views/' + (el.view || 'item.details.html');
            el.onload = function () {
                this.loaded = true;
                $("#item-details-" + this.id).removeClass('hidden');
                $("#loading-" + this.id).addClass('hidden');
            }
        };

        lazyLoad('app/scripts/data.tmp.js', function(e) {
            $scope.data = window.data;
            $scope.$apply();
        });
    }

    TableCtrl.$inject = ['$scope'];

    app.controller("TableCtrl", TableCtrl);

    // this filter converts a JSON object of styles into an injectable style attribute
    // eg. {{ {width: '50px', height: '50px'} | stylize }} == 'width: 50px; height:50px;'
    function stylize(json) {
        var output = '';

        for (var item in json) {
            if (json.hasOwnProperty(item)) {
                output += item + ':' + json[item] + ';'
            }
        }

        return output;
    }

    app.filter('stylize', function () {
        return stylize;
    });

})(angular.module('app', []));