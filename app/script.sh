INDEX=0
echo "{" > raw.json
while read line; do

	if [ "$INDEX" == "0" ]; then
		HEAD=$(echo $line | tail -1)

		echo "headers: $HEAD"
	fi

	INDEX=$((INDEX+1))

	OUT="{"
	BIT=1
	for FIELD in $(echo $line | tr ',' ' '); do
		OUT="$OUT $(echo $HEAD | cut -d',' -f${BIT}): $(echo $line | cut -d',' -f${BIT});"
		BIT=$((BIT+1))
	done

	OUT="$OUT }"

	echo $OUT >> raw.json

done < raw_lab.csv

echo "}" >> raw.json	
